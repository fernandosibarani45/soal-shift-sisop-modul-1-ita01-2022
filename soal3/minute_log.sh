#!/bin/bash

#Buat direktori log
if ! [[ -d "/home/ubuntu/log/" ]]
then
    mkdir "/home/ubuntu/log/"
fi

file_metrics="/home/ubuntu/log/metrics_$(date '+%Y%m%d%H%M%S').log"

echo mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size > $file_metrics

echo `free -m | awk '{if(NR==2) print $2","$3","$4","$5","$6","$7","} {if(NR==3) print $2","$3","$4",";}' && du -sh /home/ubuntu/ | awk '{print $2","$1}'` | awk '{ print $1 $2 $3}' >> $file_metrics

chmod 0700 $file_metrics